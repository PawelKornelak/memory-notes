/**
 * Created by RENT on 2016-08-24.
 */
const buttonToAddMessage = document.getElementById("add-message");
const htmlAmountLowMessages = document.getElementById("amount-low-type-messages");
const htmlContainerLowMessages = document.getElementById("container-low-type-messages");
const htmlAmountMediumMessages = document.getElementById("amount-medium-type-messages");
const htmlContainerMediumMessages = document.getElementById("container-medium-type-messages");
const htmlAmountHighMessages = document.getElementById("amount-high-type-messages");
const htmlContainerHighMessages = document.getElementById("container-high-type-messages");


function createMessagePart1 (){
    const title =document.getElementById("message-title").value;
    const text =document.getElementById("message-text").value;
    const type =document.querySelector('input[name="message-type"]:checked').value;
    switch (type) {
        case "low":
            var lowTypeMessage = new LowTypeMessage(title, text);
            break;
        case "medium":
            var mediumTypeMessage = new MediumTypeMessage(title, text);
            break;
        case "high":
            var highTypeMessage = new HighTypeMessage(title, text);
            break;
    }
    resetFormFieldsOnHTML();
}

var LowTypeMessage = function(title, text){
    LowTypeMessage.amontOfMessages = LowTypeMessage.amontOfMessages ? LowTypeMessage.amontOfMessages+1:1;
    createMessagePart2(title, text,htmlContainerLowMessages,LowTypeMessage,htmlAmountLowMessages);
};

var MediumTypeMessage = function(title, text){
    MediumTypeMessage.amontOfMessages = MediumTypeMessage.amontOfMessages ? MediumTypeMessage.amontOfMessages+1:1;

    createMessagePart2(title, text,htmlContainerMediumMessages,MediumTypeMessage,htmlAmountMediumMessages);
};

var HighTypeMessage = function(title, text){
    HighTypeMessage.amontOfMessages = HighTypeMessage.amontOfMessages ? HighTypeMessage.amontOfMessages+1:1;

    createMessagePart2(title, text,htmlContainerHighMessages,HighTypeMessage,htmlAmountHighMessages);
};

function createMessagePart2 (title, text,containerForMessages,objectConstructor,containerForAmountOfMessages){
    displayMessage(title, text,containerForMessages);
    displayAmountOfMessages(objectConstructor,containerForAmountOfMessages);
}

function displayMessage(title, text,htmlContainerMessages){
    let newMessageNode = document.createElement('div');
    newMessageNode.innerHTML = "<br> "+title+":<br> "+text;
    newMessageNode.id = title[0]+text[0]+Math.floor(Math.random() * 10) 																		+Math.floor(Math.random() * 10);
    htmlContainerMessages.appendChild(newMessageNode);
}

function displayAmountOfMessages(constructor,htmlAmountOfMessages) {
    htmlAmountOfMessages.innerHTML = "<b>Amount: " + constructor.amontOfMessages+"</b>";
}

function deleteMessage(event,htmlParentNode,constructor,htmlAmountOfMessages) {
    htmlParentNode.removeChild(document.getElementById(event.target.id));
    constructor.amontOfMessages--;
    displayAmountOfMessages(constructor,htmlAmountOfMessages);
}

function resetFormFieldsOnHTML(){
    document.getElementById("message-title").value="";
    document.getElementById("message-text").value="";
    document.getElementById("radio-low").checked = true;
}

buttonToAddMessage.addEventListener("click",createMessagePart1);

htmlContainerLowMessages.addEventListener("click",(event) => deleteMessage(event,htmlContainerLowMessages,LowTypeMessage,htmlAmountLowMessages));

htmlContainerMediumMessages.addEventListener("click",(event) => deleteMessage(event,htmlContainerMediumMessages,MediumTypeMessage,htmlAmountMediumMessages));

htmlContainerHighMessages.addEventListener("click",(event) => deleteMessage(event,htmlContainerHighMessages,HighTypeMessage,htmlAmountHighMessages));
